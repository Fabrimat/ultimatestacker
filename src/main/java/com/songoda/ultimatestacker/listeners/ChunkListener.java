package com.songoda.ultimatestacker.listeners;

import com.songoda.core.compatibility.ServerVersion;
import com.songoda.ultimatestacker.UltimateStacker;
import com.songoda.ultimatestacker.settings.Settings;
import com.songoda.ultimatestacker.spawner.SpawnerStack;
import org.bukkit.Chunk;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.world.ChunkLoadEvent;
import org.bukkit.event.world.ChunkUnloadEvent;

import java.lang.reflect.InvocationTargetException;
import java.util.Collection;
import java.util.HashSet;

public class ChunkListener implements Listener {

    private final UltimateStacker plugin;
    private final Collection<ChunkCoords> chunkTickets;

    public ChunkListener(UltimateStacker plugin) {
        this.plugin = plugin;
        chunkTickets = new HashSet<>();
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChunkLoad(ChunkLoadEvent e) {
        ChunkCoords chunkCoords = new ChunkCoords(e.getChunk().getX(), e.getChunk().getZ());

        if(ServerVersion.isServerVersionAtLeast(ServerVersion.V1_13)) {
            e.getChunk().addPluginChunkTicket(plugin);
        } else {
            chunkTickets.add(chunkCoords);
        }
        plugin.getDataManager().getSpawners(e.getChunk(), (spawners) -> {
            plugin.getSpawnerStackManager().addSpawners(spawners);
            if(Settings.SPAWNER_HOLOGRAMS.getBoolean()){
                plugin.loadHolograms(spawners.values());
            }

            if(ServerVersion.isServerVersionAtLeast(ServerVersion.V1_13)) {
                e.getChunk().removePluginChunkTicket(plugin);
            } else {
                chunkTickets.remove(chunkCoords);
            }
        });
    }

    @EventHandler(priority = EventPriority.MONITOR, ignoreCancelled = true)
    public void onChunkUnload(ChunkUnloadEvent e) {
        Collection<SpawnerStack> spawners = plugin.getSpawnerStackManager().getSpawners(e.getChunk());
        if(!spawners.isEmpty()){
            if(!e.getChunk().isLoaded()) {
                for(SpawnerStack spawner : spawners) {
                    plugin.getSpawnerStackManager().removeSpawner(spawner.getLocation());
                }
            }
        }
    }

    @EventHandler(priority = EventPriority.LOW, ignoreCancelled = true)
    public void onUnloadTicket(ChunkUnloadEvent e) {
        if(ServerVersion.isServerVersionBelow(ServerVersion.V1_13) && !chunkTickets.isEmpty()) {
            for(ChunkCoords coords : chunkTickets) {
                if(coords.getX() == e.getChunk().getX() && coords.getZ() == e.getChunk().getZ()){
                    try {
                        ChunkUnloadEvent.class.getMethod("setCancelled", boolean.class).invoke(e, true);
                    } catch (IllegalAccessException | InvocationTargetException | NoSuchMethodException exception) {
                        exception.printStackTrace();
                    }
                    break;
                }
            }
        }
    }

    private static class ChunkCoords {
        private final int x;
        private final int z;

        ChunkCoords(int x, int z){
            this.x = x;
            this.z = z;
        }

        public int getX() {
            return x;
        }

        public int getZ() {
            return z;
        }
    }
}
